---
name: "bundle-mac"
suites:
- "precise"
architectures:
- "i386"
packages: 
- "git-core"
- "unzip"
- "zip"
- "mingw-w64"
- "nsis"
- "faketime"
- "python"
reference_datetime: "2000-01-01 00:00:00"
remotes:
- "url": "https://git.torproject.org/tor-launcher.git"
  "dir": "tor-launcher"
- "url": "https://git.torproject.org/torbutton.git"
  "dir": "torbutton"
- "url": "https://git.torproject.org/https-everywhere.git"
  "dir": "https-everywhere"
files:
# TODO: Can we use an env for this file+version??
- "tor-browser-mac32-gbuilt.zip"
- "tor-mac32-gbuilt.zip"
- "pluggable-transports-mac32-gbuilt.zip"
- "torrc-defaults-appendix-mac"
- "relativelink-src.zip"
- "mac-skeleton.zip"
- "mac-langpacks.zip"
- "noscript@noscript.net.xpi"
- "dzip.sh"
- "bare-version"
- "bundle.inputs"
- "versions"
- "tbb-docs.zip"
- "mac-tor.sh"
script: |
  INSTDIR="$HOME/install"
  source versions
  export LIBRARY_PATH="$INSTDIR/lib"
  export LD_PRELOAD=/usr/lib/faketime/libfaketime.so.1
  export FAKETIME=$REFERENCE_DATETIME
  export TZ=UTC
  export TORBROWSER_VERSION=`cat bare-version`
  export LC_ALL=C
  umask 0022
  # 
  mkdir -p $OUTDIR/
  mkdir -p TorBrowserBundle.app/Data/Browser/profile.default/extensions/https-everywhere@eff.org
  mkdir -p TorBrowserBundle.app/Data/Browser/Caches
  mkdir -p TorBrowserBundle.app/Contents/MacOS/TorBrowser.app/Contents/MacOS/
  mkdir -p TorBrowserBundle.app/Docs/sources
  #
  cd tor-launcher
  make clean
  make package
  # FIXME: Switch to re-dzip.sh here?
  mkdir pkg/tmp
  cd pkg/tmp
  unzip ../*.xpi
  rm ../*.xpi
  ~/build/dzip.sh ../tor-launcher@torproject.org.xpi .
  mv ../tor-launcher@torproject.org.xpi ../../../TorBrowserBundle.app/Data/Browser/profile.default/extensions/tor-launcher@torproject.org.xpi
  cd ../../../
  #
  cd torbutton
  mkdir -p pkg
  ./makexpi.sh
  # FIXME: Switch to re-dzip.sh here?
  mkdir pkg/tmp
  cd pkg/tmp
  unzip ../*.xpi
  rm ../*.xpi
  ~/build/dzip.sh ../torbutton@torproject.org.xpi .
  mv ../torbutton@torproject.org.xpi ../../../TorBrowserBundle.app/Data/Browser/profile.default/extensions/torbutton@torproject.org.xpi
  cd ../../../
  #
  cd https-everywhere
  # XXX: Bloody hack to workaround a bug in HTTPS_E's git hash extraction in
  # makexpi.sh. See https://trac.torproject.org/projects/tor/ticket/10066
  rm -f .git/refs/heads/master
  ./makexpi.sh
  cp ./pkg/*.xpi ../TorBrowserBundle.app/Data/Browser/profile.default/extensions/https-everywhere@eff.org.xpi
  cd ..
  #
  cp *.xpi ./TorBrowserBundle.app/Data/Browser/profile.default/extensions/
  cd ./TorBrowserBundle.app/Data/Browser/profile.default/extensions/
  mv noscript@noscript.net.xpi {73a6fe31-595d-460b-a920-fcc0f8843232}.xpi
  cd https-everywhere@eff.org
  unzip ../https-everywhere@eff.org.xpi
  rm ../https-everywhere@eff.org.xpi
  cd ~/build/
  #
  unzip relativelink-src.zip
  cd RelativeLink
  cp RelativeLinkOSX.sh ../TorBrowserBundle.app/Contents/MacOS/TorBrowserBundle
  cd ..
  #
  unzip ~/build/tor-mac$GBUILD_BITS-gbuilt.zip
  if [ $BUILD_PT_BUNDLES ]; then
    unzip ~/build/pluggable-transports-mac$GBUILD_BITS-gbuilt.zip
  fi
  cd TorBrowserBundle.app
  unzip ~/build/mac-skeleton.zip
  unzip ~/build/tbb-docs.zip
  if [ $BUILD_PT_BUNDLES ]; then
    cat ~/build/torrc-defaults-appendix-mac >> Data/Tor/torrc-defaults
  fi
  # Install a "tor" shim that sets the working directory. See #10030.
  mv Tor/tor Tor/tor.real
  cp ~/build/mac-tor.sh Tor/tor
  cd Contents/MacOS/
  unzip ~/build/tor-browser-mac$GBUILD_BITS-gbuilt.zip
  cd ../../../
  #
  # Copy reproducibility info
  cp versions TorBrowserBundle.app/Docs/sources/
  cp bundle.inputs TorBrowserBundle.app/Docs/sources/bundle.inputs
  #
  # FF24 puts the prefs in a different jar:
  if [ -f TorBrowserBundle.app/Contents/MacOS/TorBrowser.app/Contents/MacOS/browser/omni.ja ]; then
     pushd TorBrowserBundle.app/Contents/MacOS/TorBrowser.app/Contents/MacOS/browser/
  else
     pushd TorBrowserBundle.app/Contents/MacOS/TorBrowser.app/Contents/MacOS/
  fi
  unzip omni.ja defaults/preferences/000-tor-browser.js
  cp defaults/preferences/000-tor-browser.js ~/build/
  echo "pref(\"general.useragent.locale\", \"en-US\");" >> defaults/preferences/000-tor-browser.js
  zip -Xm omni.ja defaults/preferences/000-tor-browser.js
  popd
  # XXX: DMG plz
  cp -a TorBrowserBundle.app TorBrowserBundle_en-US.app
  ~/build/dzip.sh $OUTDIR/TorBrowserBundle-${TORBROWSER_VERSION}-osx${GBUILD_BITS}_en-US.zip TorBrowserBundle_en-US.app
  rm -rf TorBrowserBundle_en-US.app
  #
  unzip mac-langpacks.zip
  cd mac-langpacks
  for i in *.xpi
  do
    LANG=`basename $i .xpi`
    cp -a ../TorBrowserBundle.app ../TorBrowserBundle_$LANG.app
    cp $i ../TorBrowserBundle_$LANG.app/Data/Browser/profile.default/extensions/langpack-$LANG@firefox.mozilla.org.xpi
    cd ..

    # FF24 puts the prefs in a different jar:
    if [ -f TorBrowserBundle_$LANG.app/Contents/MacOS/TorBrowser.app/Contents/MacOS/browser/omni.ja ]; then
       pushd TorBrowserBundle_$LANG.app/Contents/MacOS/TorBrowser.app/Contents/MacOS/browser/
    else
       pushd TorBrowserBundle_$LANG.app/Contents/MacOS/TorBrowser.app/Contents/MacOS/
    fi

    mkdir -p defaults/preferences
    cp ~/build/000-tor-browser.js defaults/preferences/000-tor-browser.js
    echo "pref(\"general.useragent.locale\", \"$LANG\");" >> defaults/preferences/000-tor-browser.js
    zip -Xm omni.ja defaults/preferences/000-tor-browser.js
    popd

    ~/build/dzip.sh $OUTDIR/TorBrowserBundle-${TORBROWSER_VERSION}-osx${GBUILD_BITS}_$LANG.zip TorBrowserBundle_$LANG.app
    rm -rf TorBrowserBundle_$LANG.app
    cd mac-langpacks
  done 
  cd ..
